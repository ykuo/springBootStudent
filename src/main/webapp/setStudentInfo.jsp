<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>SetStudentInfo</title>
	<link rel="stylesheet" href="css/font.css">
	<link rel="stylesheet" href="css/manageSys.css">
	<script src="js/jquery-3.1.1.min.js"></script>
</head>

<body>
<!--刷新页面表单-->
<form id="refresh_form" action="setStudentInfoRefresh.do" method="post"></form>

<!--遮罩层-->
<div id="mask_div"></div>

<!--增删改查按钮-->
<div id="idus_btn_div">
	<button class="idus_btn" id="insert_btn" type="button">增加</button>
	<script>
		/*增加按钮点击事件*/
		$("#insert_btn").click(function () {
			showInsertDoDiv();
		});
	</script>
	<button class="idus_btn" id="update_btn" type="button">修改</button>
	<script>
		/*保存旧学号(主键)*/
		var oldStudentNum;
		/*修改按钮点击事件*/
		$("#update_btn").click(function () {
			var radio_id = -1;//单选按钮选中位置
			/*遍历单选框获得选中id*/
			var radios = document.getElementsByName("tr_radio");
			console.log(radios);
			for (let i = 0; i < radios.length; i++) {
				if (radios[i].checked) {
					radio_id = i;
				}
			}
			if(radio_id == -1) {//没有选中radio
				alert("需要选中一条记录！");
			} else {
				oldStudentNum = $("#studentNum"+radio_id).val();
				/*选中记录赋值*/
				$("#updateStudentNum").val($("#studentNum"+radio_id).val());
				$("#updateStudentName").val($("#studentName"+radio_id).val());
				$("#updateStudentSex").val($("#studentSex"+radio_id).val());
				$("#updateStudentSdept").val($("#studentSdept"+radio_id).val());
				$("#updateStudentMajor").val($("#studentMajor"+radio_id).val());
				$("#updateStudentClassNum").val($("#studentClassNum"+radio_id).val());
				showUpdateDoDiv();
			}
		});
	</script>
	<button class="idus_btn" id="select_btn" type="button">查询</button>
	<script>
		/*查询按钮点击事件*/
		$("#select_btn").click(function () {
			showSelectDoDiv();
		});
	</script>
	<button class="idus_btn" id="delete_btn" type="button">删除</button>
	<script>
		/*删除按钮点击事件*/
		$("#delete_btn").click(function () {
			var radio_id = -1;//单选按钮选中位置
			/*遍历单选框获得选中id*/
			var radios = document.getElementsByName("tr_radio");
			console.log(radios);
			for (let i = 0; i < radios.length; i++) {
				if (radios[i].checked) {
					radio_id = i;
				}
			}
			if(radio_id == -1) {//没有选中radio
				alert("需要选中一条记录！");
			} else {
				var studentNum = $("#studentNum"+radio_id).val();
				$.ajax({//返回msg
					url:"deleteStudent.do",//请求路径
					data: {//请求数据
						"studentNum":studentNum
					},
					type:"post",//请求类型
					success:function(data) {//返回成功处理函数
						console.log(data);
						if (data == "deleteSuccess") {
							alert("删除成功！");
							/*跳转刷新*/
							$("#refresh_form").submit();
						} else if (data == "deleteFalse") {
							alert("删除失败！");
						}
					},
					error:function() {//返回失败处理函数
						console.log(data);
						alert("ajax返回失败！");
					}
				})
			}
		});
	</script>
</div>
<!--增删改查按钮end-->

<!--增加操作框-->
<div class="ius_do_div" id="insert_do_div">
	<!--操作表，每个功能不同-->
	<div class="ius_table_div">
		<table class="ius_do_table">
			<thead>
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>性别</th>
				<th>所在系</th>
				<th>专业</th>
				<th>班号</th>
			</tr>
			</thead>
			<tr class="line_tr"/>
			<tbody>
			<tr>
				<td><input class="ius_input" id="insertStudentNum"></td>
				<td><input class="ius_input" id="insertStudentName"></td>
				<td><input class="ius_input" id="insertStudentSex"></td>
				<td><input class="ius_input" id="insertStudentSdept"></td>
				<td><input class="ius_input" id="insertStudentMajor"></td>
				<td><input class="ius_input" id="insertStudentClassNum"></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--操作表end-->
	<!--增加、取消按钮-->
	<div class="ius_cancel_btns_div">
		<button class="ius_do_btn" id="insert_do_btn" type="button">增加</button>
		<script>
			/*子操作框增加按钮点击事件*/
			$("#insert_do_btn").click(function () {
				/*获取子操作表的值*/
				var studentNum = $("#insertStudentNum").val();
				var studentName = $("#insertStudentName").val();
				var studentSex = $("#insertStudentSex").val();
				var studentSdept = $("#insertStudentSdept").val();
				var studentMajor = $("#insertStudentMajor").val();
				var studentClassNum = $("#insertStudentClassNum").val();
				if (studentNum == "") {
					alert("学号不能为空！");
				} else if (studentName == "") {
					alert("姓名不能为空！");
				} else if (studentSex == "") {
					alert("性别不能为空！");
				} else if (studentSex != "男" && studentSex != "女") {
					alert("性别只能为男或女！");
				} else {
					$.ajax({//返回msg
						url:"insertStudent.do",//请求路径
						data: {//请求数据
							"studentNum":studentNum,
							"studentName":studentName,
							"studentSex":studentSex,
							"studentSdept":studentSdept,
							"studentMajor":studentMajor,
							"studentClassNum":studentClassNum
						},
						type:"post",//请求类型
						success:function(data) {//返回成功处理函数
							console.log(data);
							if (data == "studentNumExist") {
								alert("学号已存在！");
							} else if (data == "insertSuccess") {
								alert("添加成功！");
								/*跳转刷新*/
								$("#refresh_form").submit();
							} else if (data == "insertFalse") {
								alert("添加失败！");
							}
						},
						error:function() {//返回失败处理函数
							console.log(data);
							alert("ajax返回失败！");
						}
					})
				}
			});
		</script>
		<button class="ius_do_btn" id="insert_do_cancel_btn" type="button">取消</button>
	</div>
</div>
<!--增加操作框end-->

<!--修改操作框-->
<div class="ius_do_div" id="update_do_div">
	<!--操作表，每个功能不同-->
	<div class="ius_table_div">
		<table class="ius_do_table">
			<thead>
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>性别</th>
				<th>所在系</th>
				<th>专业</th>
				<th>班号</th>
			</tr>
			</thead>
			<tr class="line_tr"/>
			<tbody>
			<tr>
				<td><input class="ius_input" id="updateStudentNum"></td>
				<td><input class="ius_input" id="updateStudentName"></td>
				<td><input class="ius_input" id="updateStudentSex"></td>
				<td><input class="ius_input" id="updateStudentSdept"></td>
				<td><input class="ius_input" id="updateStudentMajor"></td>
				<td><input class="ius_input" id="updateStudentClassNum"></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--操作表end-->
	<!--修改、取消按钮-->
	<div class="ius_cancel_btns_div">
		<button class="ius_do_btn" id="update_do_btn" type="button">修改</button>
		<script>
			/*子操作框修改按钮点击事件*/
			$("#update_do_btn").click(function () {
				/*获取子操作表的值*/
				var studentNum = $("#updateStudentNum").val();
				var studentName = $("#updateStudentName").val();
				var studentSex = $("#updateStudentSex").val();
				var studentSdept = $("#updateStudentSdept").val();
				var studentMajor = $("#updateStudentMajor").val();
				var studentClassNum = $("#updateStudentClassNum").val();
				if (studentNum == "") {
					alert("学号不能为空！");
				} else if (studentName == "") {
					alert("姓名不能为空！");
				} else if (studentSex == "") {
					alert("性别不能为空！");
				} else if (studentSex != "男" && studentSex != "女") {
					alert("性别只能为男或女！");
				} else {
					$.ajax({//返回msg
						url:"updateStudent.do",//请求路径
						data: {//请求数据
							"oldStudentNum":oldStudentNum,
							"studentNum":studentNum,
							"studentName":studentName,
							"studentSex":studentSex,
							"studentSdept":studentSdept,
							"studentMajor":studentMajor,
							"studentClassNum":studentClassNum
						},
						type:"post",//请求类型
						success:function(data) {//返回成功处理函数
							console.log(data);
							if (data == "newStudentNumExist") {
								alert("新学号已存在！");
							} else {
								if (data == "updateSuccess") {
									alert("修改成功！");
									/*跳转刷新*/
									$("#refresh_form").submit();
								} else if (data == "updateFalse") {
									alert("修改失败！");
								}
							}
						},
						error:function() {//返回失败处理函数
							console.log(data);
							alert("ajax返回失败！");
						}
					})
				}
			});
		</script>
		<button class="ius_do_btn" id="update_do_cancel_btn" type="button">取消</button>
	</div>
</div>
<!--修改操作框end-->

<!--查询操作框-->
<div class="ius_do_div" id="select_do_div">
	<!--操作表，每个功能不同-->
	<div class="ius_table_div">
		<table class="ius_do_table">
			<thead>
			<tr>
				<th>学号</th>
				<th>姓名</th>
				<th>性别</th>
				<th>所在系</th>
				<th>专业</th>
				<th>班号</th>
			</tr>
			</thead>
			<tr class="line_tr"/>
			<tbody>
			<tr>
				<td><input class="ius_input" name="select_input" id="selectStudentNum"></td>
				<td><input class="ius_input" name="select_input" id="selectStudentName"></td>
				<td><input class="ius_input" name="select_input" id="selectStudentSex"></td>
				<td><input class="ius_input" name="select_input" id="selectStudentSdept"></td>
				<td><input class="ius_input" name="select_input" id="selectStudentMajor"></td>
				<td><input class="ius_input" name="select_input" id="selectStudentClassNum"></td>
			</tr>
			</tbody>
		</table>
	</div>
	<!--操作表end-->
	<!--查询、取消按钮-->
	<div class="ius_cancel_btns_div">
		<button class="ius_do_btn" id="select_do_btn" type="button">查询</button>
		<script>
			/*子操作框查询按钮点击事件*/
			$("#select_do_btn").click(function () {
				/*获取子操作表的值*/
				var studentNum = $("#selectStudentNum").val();
				var studentName = $("#selectStudentName").val();
				var studentSex = $("#selectStudentSex").val();
				var studentSdept = $("#selectStudentSdept").val();
				var studentMajor = $("#selectStudentMajor").val();
				var studentClassNum = $("#selectStudentClassNum").val();

				var inputNum = 0;/*有输入的数量*/
				if (studentNum != "") inputNum++;
				if (studentName != "") inputNum++;
				if (studentSex != "") inputNum++;
				if (studentSdept != "") inputNum++;
				if (studentMajor != "") inputNum++;
				if (studentClassNum != "") inputNum++;

				if (inputNum == 0) {//没有输入,查询所有记录
					alert("查询所有记录！");
					$("#table_title_span").html("所有记录");
					$("#tbody_storage").html("");//清空
					$.ajax({//返回所有记录
						url:"selectStudentAll.do",//请求路径
						data: {},
						type:"post",//请求类型
						dataType:"json",//响应数据返回格式
						success:function(data) {//返回成功处理函数
							console.log(data);
							$("#tbody").html("");//清空
							$.each(data,function (index,stu){
								$("#tbody").append(
										'<tr class="line_tr"/>' +
										'<tr>'+
										'<td class="radio_td">' +
										'<input class="radio" type="radio" name="tr_radio" id="'+index+'"/>'+
										'</td>'+
										'<td>'+stu.studentNum+'</td>'+
										'<td>'+stu.studentName+'</td>'+
										'<td>'+stu.studentSex+'</td>'+
										'<td>'+stu.studentSdept+'</td>'+
										'<td>'+stu.studentMajor+'</td>'+
										'<td>'+stu.studentClassNum+'</td>'+
										'</tr>'
								)
								$("#tbody_storage").append(
										'<input type="hidden" id="'+'studentNum'+index+'" value="'+stu.studentNum+'"/>'+
										'<input type="hidden" id="'+'studentName'+index+'" value="'+stu.studentName+'"/>'+
										'<input type="hidden" id="'+'studentSex'+index+'" value="'+stu.studentSex+'"/>'+
										'<input type="hidden" id="'+'studentSdept'+index+'" value="'+stu.studentSdept+'"/>'+
										'<input type="hidden" id="'+'studentMajor'+index+'" value="'+stu.studentMajor+'"/>'+
										'<input type="hidden" id="'+'studentClassNum'+index+'" value="'+stu.studentClassNum+'"/>'
								)
							})
						},
						error:function(data) {//返回失败处理函数
							console.log(data);
							alert("ajax返回失败！");
						}
					})
					hideSelectDoDiv();
				} else if (inputNum > 1) {//有多个输入
					alert("只能填写一项！");
				} else {//有1个输入
					if (studentSex != "" && (studentSex != "男" && studentSex != "女")) {//查询性别
						alert("性别只能为男或女！");
					} else {
						$("#table_title_span").html("查询结果");
						$("#tbody_storage").html("");//清空
						$.ajax({//返回查询记录
							url: "selectStudent.do",//请求路径
							data: {//请求数据
								"studentNum":studentNum,
								"studentName":studentName,
								"studentSex":studentSex,
								"studentSdept":studentSdept,
								"studentMajor":studentMajor,
								"studentClassNum":studentClassNum
							},
							dataType: "json",//响应数据返回格式
							type: "post",//请求类型
							success: function (data) {//返回成功处理函数
								console.log(data);
								if (data.length == 0) {
									alert("查询结果为空！");
								}
								else {
									alert("查询成功！");
								}
								$("#tbody").html("");//清空
								$.each(data, function (index, stu) {
									$("#tbody").append(
											'<tr class="line_tr"/>'+
											'<tr>'+
											'<td class="radio_td">'+
											'<input class="radio" type="radio" name="tr_radio" id="'+index+'"/>'+
											'</td>'+
											'<td>'+stu.studentNum+'</td>'+
											'<td>'+stu.studentName+'</td>'+
											'<td>'+stu.studentSex+'</td>'+
											'<td>'+stu.studentSdept+'</td>'+
											'<td>'+stu.studentMajor+'</td>'+
											'<td>'+stu.studentClassNum+'</td>'+
											'</tr>'
									)
									$("#tbody_storage").append(
											'<input type="hidden" id="'+'studentNum'+index+'" value="'+stu.studentNum+'"/>'+
											'<input type="hidden" id="'+'studentName'+index+'" value="'+stu.studentName+'"/>'+
											'<input type="hidden" id="'+'studentSex'+index+'" value="'+stu.studentSex+'"/>'+
											'<input type="hidden" id="'+'studentSdept'+index+'" value="'+stu.studentSdept+'"/>'+
											'<input type="hidden" id="'+'studentMajor'+index+'" value="'+stu.studentMajor+'"/>'+
											'<input type="hidden" id="'+'studentClassNum'+index+'" value="'+stu.studentClassNum+'"/>'
									)
								})
								hideSelectDoDiv();
								/*清空子操作表*/
								$("#selectStudentNum").val("");
								$("#selectStudentName").val("");
								$("#selectStudentSex").val("");
								$("#selectStudentSdept").val("");
								$("#selectStudentMajor").val("");
								$("#selectStudentClassNum").val("");
							},
							error: function () {//返回失败处理函数
								console.log(data);
								alert("ajax返回失败！");
							}
						})
					}
				}
			});
		</script>
		<button class="ius_do_btn" id="select_do_cancel_btn" type="button">取消</button>
	</div>
</div>
<!--查询操作框end-->

<!--数据表-->
<table class="table">
	<thead>
		<tr>
			<th></th>
			<th>学号</th>
			<th>姓名</th>
			<th>性别</th>
			<th>所在系</th>
			<th>专业</th>
			<th>班号</th>
		</tr>
	</thead>
	<tbody id="tbody">
	</tbody>
</table>
<!--数据表end-->

<!--保存数据表数据-->
<div type="hidden" id="tbody_storage"></div>

<script>
	$(document).ready(function () {
		$("#table_title_span").html("所有记录");
		$.ajax({//返回所有记录
			url:"selectStudentAll.do",//请求路径
			data: {},
			type:"post",//请求类型
			dataType:"json",//响应数据返回格式
			success:function(data) {//返回成功处理函数
				console.log(data);
				$.each(data,function (index,stu){
					$("#tbody").append(
						'<tr class="line_tr"/>' +
						'<tr>'+
							'<td class="radio_td">' +
								'<input class="radio" type="radio" name="tr_radio" id="'+index+'"/>'+
							'</td>'+
							'<td>'+stu.studentNum+'</td>'+
							'<td>'+stu.studentName+'</td>'+
							'<td>'+stu.studentSex+'</td>'+
							'<td>'+stu.studentSdept+'</td>'+
							'<td>'+stu.studentMajor+'</td>'+
							'<td>'+stu.studentClassNum+'</td>'+
						'</tr>'
					)
					$("#tbody_storage").append(
						'<input type="hidden" id="'+'studentNum'+index+'" value="'+stu.studentNum+'"/>'+
						'<input type="hidden" id="'+'studentName'+index+'" value="'+stu.studentName+'"/>'+
						'<input type="hidden" id="'+'studentSex'+index+'" value="'+stu.studentSex+'"/>'+
						'<input type="hidden" id="'+'studentSdept'+index+'" value="'+stu.studentSdept+'"/>'+
						'<input type="hidden" id="'+'studentMajor'+index+'" value="'+stu.studentMajor+'"/>'+
						'<input type="hidden" id="'+'studentClassNum'+index+'" value="'+stu.studentClassNum+'"/>'
					)
				})
			},
			error:function(data) {//返回失败处理函数
				console.log(data);
				alert("ajax返回失败！");
			}
		})
	});

	/*增删改查按钮鼠标移动事件*/
	$("#insert_btn").mouseenter(function () {//鼠标移入
		$("#insert_btn").css('background','#2281B0');
		$("#insert_btn").css('box-shadow','2px 2px 1px 1px #888888');
	});

	$("#insert_btn").mouseleave(function () {//鼠标移出
		$("#insert_btn").css('background','#30B4F6');
		$("#insert_btn").css('box-shadow','3px 3px 2px 2px #888888');
	});

	$("#update_btn").mouseenter(function () {//鼠标移入
		$("#update_btn").css('background','#2281B0');
		$("#update_btn").css('box-shadow','2px 2px 1px 1px #888888');
	});

	$("#update_btn").mouseleave(function () {//鼠标移出
		$("#update_btn").css('background','#30B4F6');
		$("#update_btn").css('box-shadow','3px 3px 2px 2px #888888');
	});

	$("#select_btn").mouseenter(function () {//鼠标移入
		$("#select_btn").css('background','#2281B0');
		$("#select_btn").css('box-shadow','2px 2px 1px 1px #888888');
	});

	$("#select_btn").mouseleave(function () {//鼠标移出
		$("#select_btn").css('background','#30B4F6');
		$("#select_btn").css('box-shadow','3px 3px 2px 2px #888888');
	});

	$("#delete_btn").mouseenter(function () {//鼠标移入
		$("#delete_btn").css('background','#2281B0');
		$("#delete_btn").css('box-shadow','2px 2px 1px 1px #888888');
	});

	$("#delete_btn").mouseleave(function () {//鼠标移出
		$("#delete_btn").css('background','#30B4F6');
		$("#delete_btn").css('box-shadow','3px 3px 2px 2px #888888');
	});

	/*增加子输入框展示/隐藏函数*/
	function showInsertDoDiv() {
		$("#mask_div").css('display','inherit');
		$("#insert_do_div").css('display','inherit');
	}

	function hideInsertDoDiv() {
		$("#mask_div").css('display','none');
		$("#insert_do_div").css('display','none');
	}

	/*修改子输入框展示/隐藏函数*/
	function showUpdateDoDiv() {
		$("#mask_div").css('display','inherit');
		$("#update_do_div").css('display','inherit');
	}

	function hideUpdateDoDiv() {
		$("#mask_div").css('display','none');
		$("#update_do_div").css('display','none');
	}

	/*查询子输入框展示/隐藏函数*/
	function showSelectDoDiv() {
		$("#mask_div").css('display','inherit');
		$("#select_do_div").css('display','inherit');
	}

	function hideSelectDoDiv() {
		$("#mask_div").css('display','none');
		$("#select_do_div").css('display','none');
		$("#select_do_div").css('display','none');
	}

	/*增加、修改、查询和取消按钮鼠标移动事件*/
	$("#insert_do_btn").mouseenter(function () {//鼠标移入
		$("#insert_do_btn").css('background','#CFCFCF');
	});

	$("#insert_do_btn").mouseleave(function () {//鼠标移出
		$("#insert_do_btn").css('background','#F2F2F2');
	});

	$("#insert_do_cancel_btn").mouseenter(function () {//鼠标移入
		$("#insert_do_cancel_btn").css('background','#CFCFCF');
	});

	$("#insert_do_cancel_btn").mouseleave(function () {//鼠标移出
		$("#insert_do_cancel_btn").css('background','#F2F2F2');
	});

	$("#update_do_btn").mouseenter(function () {//鼠标移入
		$("#update_do_btn").css('background','#CFCFCF');
	});

	$("#update_do_btn").mouseleave(function () {//鼠标移出
		$("#update_do_btn").css('background','#F2F2F2');
	});

	$("#update_do_cancel_btn").mouseenter(function () {//鼠标移入
		$("#update_do_cancel_btn").css('background','#CFCFCF');
	});

	$("#update_do_cancel_btn").mouseleave(function () {//鼠标移出
		$("#update_do_cancel_btn").css('background','#F2F2F2');
	});

	$("#select_do_btn").mouseenter(function () {//鼠标移入
		$("#select_do_btn").css('background','#CFCFCF');
	});

	$("#select_do_btn").mouseleave(function () {//鼠标移出
		$("#select_do_btn").css('background','#F2F2F2');
	});

	$("#select_do_cancel_btn").mouseenter(function () {//鼠标移入
		$("#select_do_cancel_btn").css('background','#CFCFCF');
	});

	$("#select_do_cancel_btn").mouseleave(function () {//鼠标移出
		$("#select_do_cancel_btn").css('background','#F2F2F2');
	});

	/*子操作框取消按钮点击事件*/
	$("#insert_do_cancel_btn").click(function () {
		hideInsertDoDiv();
	});

	$("#update_do_cancel_btn").click(function () {
		hideUpdateDoDiv();
	});

	$("#select_do_cancel_btn").click(function () {
		hideSelectDoDiv();
	});
</script>
</body>
</html>