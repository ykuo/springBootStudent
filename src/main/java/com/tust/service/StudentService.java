package com.tust.service;

import com.tust.domain.Student;

import java.util.List;

public interface StudentService {
    int insertStudent(Student student);
    int deleteStudent(String studentNum);
    int updateStudent(String oldStudentNum, Student student);
    List<Student> selectStudentByStudentNum(String studentNum);
    List<Student> selectStudentByStudentName(String studentName);
    List<Student> selectStudentByStudentSex(String studentSex);
    List<Student> selectStudentByStudentSdept(String studentSdept);
    List<Student> selectStudentByStudentMajor(String studentMajor);
    List<Student> selectStudentByStudentClassNum(String studentClassNum);
    List<Student> selectStudentAll();
}
