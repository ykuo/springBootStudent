package com.tust.service.impl;

import com.tust.dao.StudentDao;
import com.tust.domain.Student;
import com.tust.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentDao studentDao;

    @Override
    public int insertStudent(Student student) {
        return studentDao.insertStudent(student);
    }

    @Override
    public int deleteStudent(String studentNum) {
        return studentDao.deleteStudent(studentNum);
    }

    @Override
    public int updateStudent(String oldStudentNum, Student student) {
        return studentDao.updateStudent(oldStudentNum, student);
    }

    @Override
    public List<Student> selectStudentByStudentNum(String studentNum) {
        return studentDao.selectStudentByStudentNum(studentNum);
    }

    @Override
    public List<Student> selectStudentByStudentName(String studentName) {
        return studentDao.selectStudentByStudentName(studentName);
    }

    @Override
    public List<Student> selectStudentByStudentSex(String studentSex) {
        return studentDao.selectStudentByStudentSex(studentSex);
    }

    @Override
    public List<Student> selectStudentByStudentSdept(String studentSdept) {
        return studentDao.selectStudentByStudentSdept(studentSdept);
    }

    @Override
    public List<Student> selectStudentByStudentMajor(String studentMajor) {
        return studentDao.selectStudentByStudentMajor(studentMajor);
    }

    @Override
    public List<Student> selectStudentByStudentClassNum(String studentClassNum) {
        return studentDao.selectStudentByStudentClassNum(studentClassNum);
    }

    @Override
    public List<Student> selectStudentAll() {
        return studentDao.selectStudentAll();
    }
}
