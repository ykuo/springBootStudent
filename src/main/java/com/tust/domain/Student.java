package com.tust.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String studentNum;
    private String studentName;
    private String studentSex;
    private String studentSdept;
    private String studentMajor;
    private String studentClassNum;
}
