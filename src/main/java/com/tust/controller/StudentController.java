package com.tust.controller;

import com.tust.domain.Student;
import com.tust.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class StudentController {
    @Autowired
    private StudentService studentService;

    @ResponseBody
    @RequestMapping("/selectStudentAll.do")
    public List<Student> selectStudentAll(String a) {
        List<Student> students = studentService.selectStudentAll();
        //System.out.println(students.toString());
        return students;
    }

    @ResponseBody
    @RequestMapping("/insertStudent.do")
    public String insertStudent(String studentNum, String studentName,
                                String studentSex, String studentSdept,
                                String studentMajor, String studentClassNum) {
        String msg;
        List<Student> student = studentService.selectStudentByStudentNum(studentNum);
        if (student.size() == 1) {//学号存在
            msg = "studentNumExist";
        } else {
            Student newStudent = new Student(studentNum, studentName,
                    studentSex, studentSdept, studentMajor, studentClassNum);
            if (studentService.insertStudent(newStudent) == 1) {//插入成功
                msg = "insertSuccess";
            } else {//插入失败
                msg = "insertFalse";
            }
        }
        System.out.println(msg);
        return msg;
    }

    @ResponseBody
    @RequestMapping("/updateStudent.do")
    public String updateStudent(String oldStudentNum, String studentNum,
                                String studentName, String studentSex,
                                String studentSdept, String studentMajor,
                                String studentClassNum) {
        String msg;
        List<Student> student = studentService.selectStudentByStudentNum(studentNum);
        if (student.size() == 1 && !oldStudentNum.equals(studentNum)) {//新学号存在
            msg = "newStudentNumExist";
        } else {
            Student newStudent = new Student(studentNum, studentName,
                    studentSex, studentSdept, studentMajor, studentClassNum);
            if (studentService.updateStudent(oldStudentNum, newStudent) == 1) {//修改成功
                msg = "updateSuccess";
            } else {//修改失败
                msg = "updateFalse";
            }
        }
        return msg;
    }

    @ResponseBody
    @RequestMapping("/selectStudent.do")
    public List<Student> selectStudent(String studentNum, String studentName,
                                       String studentSex, String studentSdept,
                                       String studentMajor, String studentClassNum) {
        List<Student> student = null;
        if (!studentNum.equals("")) {//查询学号
            student = studentService.selectStudentByStudentNum(studentNum);
        } else if (!studentName.equals("")) {//查询学生姓名
            student = studentService.selectStudentByStudentName(studentName);
        } else if (!studentSex.equals("")) {//查询性别
            student = studentService.selectStudentByStudentSex(studentSex);
        } else if (!studentSdept.equals("")) {//查询所在系
            student = studentService.selectStudentByStudentSdept(studentSdept);
        } else if (!studentMajor.equals("")) {//查询专业
            student = studentService.selectStudentByStudentMajor(studentMajor);
        } else if (!studentClassNum.equals("")) {//查询班号
            student = studentService.selectStudentByStudentClassNum(studentClassNum);
        }
        return student;
    }

    @ResponseBody
    @RequestMapping("/deleteStudent.do")
    public String deleteStudent(String studentNum) {
        String msg;
        if (studentService.deleteStudent(studentNum) == 1) {//删除成功
            msg = "deleteSuccess";
        } else {//删除失败
            msg = "deleteFalse";
        }
        return msg;
    }

    @RequestMapping("/setStudentInfoRefresh.do")
    public ModelAndView setStudentInfoRefresh() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("setStudentInfo.jsp");
        return mv;
    }
}
